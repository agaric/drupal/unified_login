<?php

namespace Drupal\unified_login\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Drupal\unified_login\Controller\LoginController;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    // Unify the login/register pages.
    $login_route = $collection->get('user.login');
    $register_route = $collection->get('user.register');
    if ($login_route && $register_route) {
      $login_route->setDefault('_controller', LoginController::class . '::page');
      $login_route->setDefault('_login_form', $login_route->getDefault('_form'));
      $login_route->setDefault('_register_form', $register_route->getDefault('_entity_form'));

      $register_route->setDefault('_controller', LoginController::class . '::page');
      $register_route->setDefault('_login_form', $login_route->getDefault('_form'));
      $register_route->setDefault('_register_form', $register_route->getDefault('_entity_form'));
    }

  }

}
